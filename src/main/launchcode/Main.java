package launchcode;

public class Main {

    //Create/update
    public static void main(String[] args) {

        //Run binary Search
        int n[] = new int[]{1 ,2,3,4,5,6,7,20,92, 903, 1000};
        for (int i = 0; i<n.length; i++){
            System.out.println("Values of n("+i+"): " + n[i]);
        }

        //Call binarySearch() method
        int result = BinarySearch.binarySearch(n, 92);
        System.out.println("\n********* Binary Search ***********");
        System.out.println("Result expected should be 8 when searching for 92.");
        System.out.println("Search Index Value: "+ result);

        //Call hasBalancedBrackets method
        System.out.println("\n***********Check BalancedBrakets **********");
        System.out.println("Result expected should be true when passing [for] within a string statement.");
        System.out.println("true: "+ BalancedBrackets.hasBalancedBrackets("this is a test [for] JUNIT test."));

        //Call equals method from Fraction
        Fraction fraction1 = new Fraction(6, 30);
        Fraction fraction2 = new Fraction(1, 5);
        Fraction fraction3 = new Fraction(3, 15);
        System.out.println(fraction1.equals(fraction2)); //return true
        System.out.println(fraction2.equals(fraction3)); //return true
        System.out.println(fraction1.compareTo(fraction3)); //return 0
        System.out.println(fraction2.compareTo(fraction3)); //return 0

        //Call RomanNumeral.fromInt() method
        System.out.println(RomanNumeral.fromInt(51)); //return LI
    }
}
