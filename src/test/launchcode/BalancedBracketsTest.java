package launchcode;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class BalancedBracketsTest {

    @Test
    public void testBinarySearch(){
        //Assuming given an array as follows:
        int n[] = new int[]{1 ,2,3,4,5,6,7,20,92, 903, 1000};
        Assert.assertEquals(8, BinarySearch.binarySearch(n, 92));
    }

    @Test
    public void hasBalancedBrackets(){
        //System.out.println("test true/false: " + BalancedBrackets.hasBalancedBrackets("this is a test [for] JUNIT test."));
        Assert.assertEquals(true, BalancedBrackets.hasBalancedBrackets("this is a test [for] JUNIT test."));
    }

    @Test
    public void testFraction(){
        Fraction fraction1 = new Fraction(6, 30);
        Fraction fraction2 = new Fraction(1, 5);
        Fraction fraction3 = new Fraction(3, 15);
        Fraction fraction4 = new Fraction(1, 4);
        Assert.assertEquals(true, fraction1.equals(fraction2));
        Assert.assertEquals(true, fraction2.equals(fraction3));
        Assert.assertEquals(0, fraction1.compareTo(fraction2));
        Assert.assertEquals(0, fraction2.compareTo(fraction3));
        Assert.assertTrue(fraction4.compareTo(fraction2)<0);
    }

    @Test
    public void testRomanNumeral(){
        //System.out.println(RomanNumeral.fromInt(51));
        Assert.assertEquals("LI", RomanNumeral.fromInt(51));
        Assert.assertEquals("XII", RomanNumeral.fromInt(12));
        Assert.assertNotEquals("V", RomanNumeral.fromInt(4));
    }


}
